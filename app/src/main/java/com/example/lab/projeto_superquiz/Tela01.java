package com.example.lab.projeto_superquiz;

import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Tela01 extends AppCompatActivity implements View.OnClickListener, Runnable {

    private TextView questao;
    private Button btn1, btn2, btn3, btn4;
    private TextView txt, pontos, tempo;
    private String Recebe;
    private ArrayList<Apoio> lista;
    private int quemEstaCerto;
    private String textoRecebido, gabaritosRecebidos;
    private String nome;
    private int atvAtual, atvMax, indiceLista, dificuldadeTempo;
    private Handler handler, h;
    private boolean primeiraVez;
    private int Recebe1;
    private CountDownTimer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela01);

        primeiraVez = true;
        indiceLista = 0;
        Recebe1 = 10;


        handler = new Handler();
        atvAtual = 0;
        //atvMax = Recebe1;
        // MAX ira receber o quantitativo selecionado na tela anterior
        txt = (TextView) findViewById(R.id.textView3);
        Intent i = getIntent();
        if (i != null) {
            Bundle b = new Bundle();
            b = i.getExtras();
            if (b != null) {
                Recebe = b.getString("Nome", null);
                nome = b.getString("Nome", null);
                txt.setText(Recebe);
                dificuldadeTempo = b.getInt("Dificuldade", 61000);
                Recebe1 = b.getInt("valor", 10);
                atvMax = Recebe1;
                ///atvMax = b.getString("valor", null);
               // txt.setText(Recebe1);
            }

        }


        pontos = (TextView) findViewById(R.id.textView6);
        tempo = (TextView) findViewById(R.id.textView4);


        btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(this);

        btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(this);

        btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(this);

        btn4 = (Button) findViewById(R.id.btn4);
        btn4.setOnClickListener(this);

        questao = (TextView) findViewById(R.id.textView);

        carregarElementos();
        iniciarJogo(primeiraVez);
    }

    private void carregarElementos() {


        lista = new ArrayList<Apoio>();

        //leitura do arquivo QUESTÕES
        InputStream in = getResources().openRawResource(R.raw.questoes);
        try {
            int tamanho = in.available();
            byte bytes[] = new byte[tamanho];
            in.read(bytes);
            // # ESTA STRING ESTA RECEBENDO O ARQUIVO COMPLETO DE QUESTÕES
            textoRecebido = new String(bytes);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //leitura do arquivo GABARITO
        InputStream in2 = getResources().openRawResource(R.raw.gabarito);
        try {
            int tamanho = in2.available();
            byte bytes[] = new byte[tamanho];
            in2.read(bytes);
            // # ESTA STRING ESTA RECEBENDO O ARQUIVO COMPLETO COM GABARITOS
            gabaritosRecebidos = new String(bytes);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // # A STRING textoRecebido É QUEBRADA EM UM VETOR, CADA POSIÇÃO TEM A QUESTÃO E AS RESPOSTAS
        String temp[] = textoRecebido.split("#");

        String gabs[] = gabaritosRecebidos.split("#");


        for (int i = 0; i < temp.length; i++) {
            Apoio a = new Apoio();

            // #  CAPTURANDO AS QUESTÕES
            int posicaoInterrogacao = temp[i].lastIndexOf('?');
            String questao = new String(temp[i].substring(0, posicaoInterrogacao + 1));
            a.setQuestao(questao);

            // # CAPTURANDO TODAS AS RESPOSTAS
            int ultimaPosicao = temp[i].length() - 1;
            String respostas = new String();
            respostas = new String(temp[i].substring(posicaoInterrogacao + 1, ultimaPosicao));

            // # SEPARANDO AS RESPOSTAS
            String vetRespostas[] = respostas.split("&");

            a.setResposta1(vetRespostas[0]);
            a.setResposta2(vetRespostas[1]);
            a.setResposta3(vetRespostas[2]);
            a.setResposta4(vetRespostas[3]);
            a.setIndiceCorreta(gabs[i]);

            lista.add(a);
        }

    }

    private void iniciarJogo(boolean b) {


     /*   h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                btn1.setBackgroundColor(Color.RED);
                btn2.setBackgroundColor(Color.RED);
                btn3.setBackgroundColor(Color.RED);
                btn4.setBackgroundColor(Color.RED);
                iniciarJogo(primeiraVez);
            }
        }, dificuldadeTempo);
        */


        if(timer != null){
            timer.cancel();

        }
        timer = new CountDownTimer(dificuldadeTempo, 1000) {

            public void onTick(long millisUntilFinished) {
                tempo.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                Handler han = new Handler();
                btn1.setBackgroundColor(Color.RED);
                btn2.setBackgroundColor(Color.RED);
                btn3.setBackgroundColor(Color.RED);
                btn4.setBackgroundColor(Color.RED);
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iniciarJogo(primeiraVez);
                    }
                }, 1500);
            }
        }.start();


        btn1.setBackgroundColor(Color.WHITE);
        btn2.setBackgroundColor(Color.WHITE);
        btn3.setBackgroundColor(Color.WHITE);
        btn4.setBackgroundColor(Color.WHITE);

        if (b) {
            primeiraVez = false;
            Collections.shuffle(lista);
        }

        questao.setText(lista.get(indiceLista).getQuestao());
        btn1.setText(lista.get(indiceLista).getResposta1());
        btn2.setText(lista.get(indiceLista).getResposta2());
        btn3.setText(lista.get(indiceLista).getResposta3());
        btn4.setText(lista.get(indiceLista).getResposta4());

        if (lista.get(indiceLista).getIndiceCorreta().compareToIgnoreCase("1") == 0) {
            quemEstaCerto = 1;
        } else if (lista.get(indiceLista).getIndiceCorreta().compareToIgnoreCase("2") == 0) {
            quemEstaCerto = 2;
        } else if (lista.get(indiceLista).getIndiceCorreta().compareToIgnoreCase("3") == 0) {
            quemEstaCerto = 3;
        } else {
            quemEstaCerto = 4;
        }

        indiceLista++;

    }

    public void onClick(View view) {
        if (view == btn1) {

            if (quemEstaCerto == 1) {
                Log.d("##", "Aqui");
                btn1.setBackgroundColor(Color.GREEN);
                btn2.setBackgroundColor(Color.RED);
                btn3.setBackgroundColor(Color.RED);
                btn4.setBackgroundColor(Color.RED);
                int pt = Integer.parseInt((pontos.getText().toString()));
                pt += 1;
                pontos.setText(Integer.toString(pt));

            } else {
                if(quemEstaCerto == 2){
                    btn1.setBackgroundColor(Color.RED);
                    btn2.setBackgroundColor(Color.GREEN);
                    btn3.setBackgroundColor(Color.RED);
                    btn4.setBackgroundColor(Color.RED);
                } else {
                    if(quemEstaCerto == 3){
                        btn1.setBackgroundColor(Color.RED);
                        btn2.setBackgroundColor(Color.RED);
                        btn3.setBackgroundColor(Color.GREEN);
                        btn4.setBackgroundColor(Color.RED);
                    } else {
                        if(quemEstaCerto == 4){
                            btn1.setBackgroundColor(Color.RED);
                            btn2.setBackgroundColor(Color.RED);
                            btn3.setBackgroundColor(Color.RED);
                            btn4.setBackgroundColor(Color.GREEN);
                        }
                    }
                }
                //colocar os demais botoes nas cores, sendo 2 em vermelho e 1 verde
                //if para descobrir onde esta ´certo
                //pontos não altera
                //alrerar atividade
                //chamar o handler com 4 segundops
            }
        }

        if (view == btn2) {

            if (quemEstaCerto == 2) {
                Log.d("##", "Aqui2");

                btn1.setBackgroundColor(Color.RED);
                btn2.setBackgroundColor(Color.GREEN);
                btn3.setBackgroundColor(Color.RED);
                btn4.setBackgroundColor(Color.RED);
                int pt = Integer.parseInt((pontos.getText().toString()));
                pt += 1;
                pontos.setText(Integer.toString(pt));

            } else {
                btn2.setBackgroundColor(Color.RED);
                if(quemEstaCerto == 1){
                    btn1.setBackgroundColor(Color.GREEN);
                    btn2.setBackgroundColor(Color.RED);
                    btn3.setBackgroundColor(Color.RED);
                    btn4.setBackgroundColor(Color.RED);
                } else {
                    if(quemEstaCerto == 3){
                        btn1.setBackgroundColor(Color.RED);
                        btn2.setBackgroundColor(Color.RED);
                        btn3.setBackgroundColor(Color.GREEN);
                        btn4.setBackgroundColor(Color.RED);
                    } else {
                        if(quemEstaCerto == 4){
                            btn1.setBackgroundColor(Color.RED);
                            btn2.setBackgroundColor(Color.RED);
                            btn3.setBackgroundColor(Color.RED);
                            btn4.setBackgroundColor(Color.GREEN);
                        }
                    }
                }
                //colocar os demais botoes nas cores, sendo 2 em vermelho e 1 verde
                //if para descobrir onde esta ´certo
                //pontos não altera
                //alrerar atividade
                //chamar o handler com 4 segundops
            }
        }

        if (view == btn3) {

            if (quemEstaCerto == 3) {
                Log.d("##", "Aqui3");

                btn1.setBackgroundColor(Color.RED);
                btn2.setBackgroundColor(Color.RED);
                btn3.setBackgroundColor(Color.GREEN);
                btn4.setBackgroundColor(Color.RED);
                int pt = Integer.parseInt((pontos.getText().toString()));
                pt += 1;
                pontos.setText(Integer.toString(pt));

            } else {
                btn3.setBackgroundColor(Color.RED);
                if(quemEstaCerto == 2){
                    btn1.setBackgroundColor(Color.RED);
                    btn2.setBackgroundColor(Color.GREEN);
                    btn3.setBackgroundColor(Color.RED);
                    btn4.setBackgroundColor(Color.RED);
                } else {
                    if(quemEstaCerto == 1){
                        btn1.setBackgroundColor(Color.GREEN);
                        btn2.setBackgroundColor(Color.RED);
                        btn3.setBackgroundColor(Color.RED);
                        btn4.setBackgroundColor(Color.RED);
                    } else {
                        if(quemEstaCerto == 4){
                            btn1.setBackgroundColor(Color.RED);
                            btn2.setBackgroundColor(Color.RED);
                            btn3.setBackgroundColor(Color.RED);
                            btn4.setBackgroundColor(Color.GREEN);
                        }
                    }
                }
                //colocar os demais botoes nas cores, sendo 2 em vermelho e 1 verde
                //if para descobrir onde esta ´certo
                //pontos não altera
                //alrerar atividade
                //chamar o handler com 4 segundops
            }
        }

        if (view == btn4) {

            if (quemEstaCerto == 4) {
                Log.d("##", "Aqui4");

                btn1.setBackgroundColor(Color.RED);
                btn2.setBackgroundColor(Color.RED);
                btn3.setBackgroundColor(Color.RED);
                btn4.setBackgroundColor(Color.GREEN);
                int pt = Integer.parseInt((pontos.getText().toString()));
                pt += 1;
                pontos.setText(Integer.toString(pt));

            } else {
                btn4.setBackgroundColor(Color.RED);
                if(quemEstaCerto == 2){
                    btn1.setBackgroundColor(Color.RED);
                    btn2.setBackgroundColor(Color.GREEN);
                    btn3.setBackgroundColor(Color.RED);
                    btn4.setBackgroundColor(Color.RED);
                } else {
                    if(quemEstaCerto == 3){
                        btn1.setBackgroundColor(Color.RED);
                        btn2.setBackgroundColor(Color.RED);
                        btn3.setBackgroundColor(Color.GREEN);
                        btn4.setBackgroundColor(Color.RED);
                    } else {
                        if(quemEstaCerto == 1){
                            btn1.setBackgroundColor(Color.GREEN);
                            btn2.setBackgroundColor(Color.RED);
                            btn3.setBackgroundColor(Color.RED);
                            btn4.setBackgroundColor(Color.RED);
                        }
                    }
                }
                //colocar os demais botoes nas cores, sendo 2 em vermelho e 1 verde
                //if para descobrir onde esta ´certo
                //pontos não altera
                //alterar atividade
                //chamar o handler com 4 segundos
            }
        }
        atvAtual++;
        if (atvAtual < atvMax) {
            handler.postDelayed(this, 1500);
        } else {
            Intent i = new Intent(this, Tela02.class);
            Bundle bun = new Bundle();

            bun.putString("Nome", nome);
            double b = Double.parseDouble(pontos.getText().toString());
            bun.putDouble("Pontos", b);
            double d = atvMax;
            bun.putDouble("AtvMax", d);

            i.putExtras(bun);
            startActivity(i);
            //se ja atingit o limite maximo
        }


    }

        @Override
        public void run() {
            iniciarJogo(primeiraVez);

        }
}

