package com.example.lab.projeto_superquiz;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Tela0 extends Activity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{

    private Button btnAvancar;
    private EditText txtNome;
    private RadioButton rBtn10, rBtn20, rBtn30, rBtn40, rBtn50, rBtnFacil, rBtnMedio, rBtnDificil;
    private RadioGroup grupo, dificuldade;
    private int qtdquestoes;
    private int dificuldadeTempo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela0);

        qtdquestoes = 10;

        txtNome = (EditText) findViewById(R.id.txtNome);
        btnAvancar = (Button) findViewById(R.id.btnAvancar);

        rBtnFacil = (RadioButton) findViewById(R.id.rBtnfacil);
        rBtnMedio = (RadioButton) findViewById(R.id.rBtnmedio);
        rBtnDificil = (RadioButton) findViewById(R.id.rBtndificil);

        rBtn10 = (RadioButton) findViewById(R.id.rBtn10);
        rBtn20 = (RadioButton) findViewById(R.id.rBtn20);
        rBtn30 = (RadioButton) findViewById(R.id.rBtn30);
        rBtn40 = (RadioButton) findViewById(R.id.rBtn40);
        rBtn50 = (RadioButton) findViewById(R.id.rBtn50);

        btnAvancar.setOnClickListener(this);

        dificuldade = (RadioGroup) findViewById(R.id.dificuldade);
        dificuldade.setOnCheckedChangeListener(this);

        grupo = (RadioGroup) findViewById(R.id.grupo);
        grupo.setOnCheckedChangeListener(this);
    }

    public void onClick(View view) {

        String nome = txtNome.getText().toString();

        if(view == btnAvancar){
            Intent i = new Intent(this, Tela01.class);
            Bundle b = new Bundle();

            b.putString("Nome", nome);
            b.putInt("valor", qtdquestoes);
            b.putInt("Dificuldade", dificuldadeTempo);

            i.putExtras(b);
            startActivity(i);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i)
    {
        if(radioGroup == grupo){
            if(i == rBtn10.getId()) {
                qtdquestoes = 10;
            }
            if(i == rBtn20.getId()) {
                qtdquestoes = 20;
            }
            if(i == rBtn30.getId()) {
                qtdquestoes = 30;
            }
            if(i == rBtn40.getId()) {
                qtdquestoes = 40;
            }
            if(i == rBtn50.getId()) {
                qtdquestoes = 50;
            }
        }
         if (radioGroup == dificuldade){
            if(i == rBtnFacil.getId()){
                dificuldadeTempo = 61000;
            }
             if(i == rBtnMedio.getId()){
                 dificuldadeTempo = 46000;
             }
             if(i == rBtnDificil.getId()){
                 dificuldadeTempo = 31000;
             }
         }

    }

}
