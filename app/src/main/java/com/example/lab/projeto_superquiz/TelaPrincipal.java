package com.example.lab.projeto_superquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaPrincipal extends AppCompatActivity implements View.OnClickListener {

    private Button comecar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        comecar = (Button)findViewById(R.id.button);
        comecar.setOnClickListener(this);
    }
    public void onClick(View view){
        if(view == comecar){
            Intent i = new Intent(this,Tela0.class);
            startActivity(i);
        }
    }
}
