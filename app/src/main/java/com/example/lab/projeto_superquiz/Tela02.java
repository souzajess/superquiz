package com.example.lab.projeto_superquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import pl.droidsonroids.gif.GifImageView;

public class Tela02 extends AppCompatActivity implements View.OnClickListener {

    private Button btnRestart;
    private TextView txt;
    private TextView pttxt;
    private TextView titulo;
    private String Recebe;
    private int RecebePt, RecebeAtv;
    private double finalResult;
    private double a, b;
    private GifImageView gif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela02);

        finalResult = 0.1;
        txt = (TextView)findViewById(R.id.textView13);
        pttxt = (TextView)findViewById(R.id.textView14);
        gif = (GifImageView) findViewById(R.id.id1);

        Intent i = getIntent();
        if (i != null){
            Bundle b1 = new Bundle();
            b1 = i.getExtras();
            if (b1 != null){
                Recebe = b1.getString("Nome",null);
                txt.setText(Recebe);
                String temp;
                a = b1.getDouble("Pontos",0.0);
              //  RecebePt = Integer.parseInt(temp);
                int c  =  (int) a;
                pttxt.setText(Integer.toString(c));
                b = b1.getDouble("AtvMax", 0.0);
            }
        }

        finalResult = (a/b) * 100 ;
        Log.d("## final", Double.toString(finalResult));
        Log.d("## recebePT", Double.toString(a));
        Log.d("## Recebeatv", Double.toString(b));


        if (finalResult == 100){
            titulo = (TextView)findViewById(R.id.textView16);
            titulo.setText("SuperAluno");
            gif.setImageResource(R.drawable.superaluno2);
        } else {
            if (finalResult >= 75 && finalResult <= 99){
                titulo = (TextView)findViewById(R.id.textView16);
                titulo.setText("Excelente");
                gif.setImageResource(R.drawable.excelente);
            } else {
                if (finalResult >= 50 && finalResult < 75){
                    titulo = (TextView)findViewById(R.id.textView16);
                    titulo.setText("Bom");
                    gif.setImageResource(R.drawable.bom);
                } else {
                    if (finalResult >= 25 && finalResult < 50){
                        titulo = (TextView)findViewById(R.id.textView16);
                        titulo.setText("Esforçado");
                        gif.setImageResource(R.drawable.esforcado);
                    } else {
                        if (finalResult >= 1 && finalResult < 25){
                            titulo = (TextView)findViewById(R.id.textView16);
                            titulo.setText("Precisa Estudar");
                            gif.setImageResource(R.drawable.dc3e8c35862413);
                        } else {
                            if (finalResult == 0){
                                titulo = (TextView)findViewById(R.id.textView16);
                                titulo.setText("Kryptonita");
                                gif.setImageResource(R.drawable.kryptonita);

                            }
                        }
                    }
                }
            }
        }

        btnRestart = (Button)findViewById(R.id.button2);
        btnRestart.setOnClickListener(this);

        try {
            FileOutputStream out = openFileOutput(arquivo, MODE_PRIVATE);
            String pergunta = "qual eh a musica";
            String alternativaA = "tipo a";
            out.write(pergunta.getBytes());
            out.write("\n".getBytes());
            out.write(alternativaA.getBytes());

            String texto = ed1.getText().toString();
            out.write("\n".getBytes());
            out.write(texto.getBytes());
            out.close();
            ed1.setText("");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onClick(View view){
        if(view == btnRestart ){
            Intent i = new Intent(this,Tela0.class);
            startActivity(i);
        }
    }
}
